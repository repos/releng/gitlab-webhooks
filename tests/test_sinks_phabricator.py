# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import logging
import pathlib

import aioresponses
import pytest

from glwebhooks import settings
import glwebhooks.sinks.phabricator

data_path = pathlib.Path(__file__).parent / "data"
logger = logging.getLogger(__name__)


@pytest.fixture
def sink():
    return glwebhooks.sinks.phabricator.PhabricatorSink(None)


def mock_aiohttp_post(mocker, path, **kwargs):
    """Add a mock aiohttp POST response."""
    return mocker.post(
        f"{settings.PHABRICATOR_URL}/api/{path}",
        **kwargs,
    )


def conduit_response(result, error_code=None):
    """Build a Conduit response object."""
    return {
        "error_code": error_code,
        "result": result,
    }


def build_request_data(data):
    """Build a phab.Client request payload."""
    data["__conduit__"] = {"token": "DUMMY_VALUE_FOR_TESTS"}
    return {
        "params": json.dumps(data),
        "output": "json",
    }


async def test_notify_mr_open(sink):
    data = data_path / "event_merge_request_open.json"
    event = json.load(data.open())

    expected_task = "T341719"
    expected_phid = "PHID-TASK-w7ugufo46wm6ppmjypnb"
    expected_comment = (
        "taavi opened "
        "https://gitlab.wikimedia.org/repos/ci-tools/libup/"
        "-/merge_requests/37\n\n"
        "runner: Include .vue for default stylelint glob\n"
    )
    expected_transactions = [
        {"type": "comment", "value": expected_comment},
        {
            "type": "projects.add",
            "value": [
                glwebhooks.sinks.phabricator.PROJECT_PATCH_FOR_REVIEW_PHID,
            ],
        },
    ]

    with aioresponses.aioresponses() as m:
        mock_aiohttp_post(
            m,
            "phid.lookup",
            payload=conduit_response(
                {
                    expected_task: {
                        "type": "TASK",
                        "phid": expected_phid,
                    },
                },
            ),
        )
        mock_aiohttp_post(
            m,
            "maniphest.query",
            payload=conduit_response(
                {
                    expected_phid: {
                        "auxiliary": {
                            "std:maniphest:security_topic": "default",
                        },
                    },
                },
            ),
        )
        mock_aiohttp_post(m, "maniphest.edit", payload=conduit_response({}))

        await sink.notify(event)
        assert len(m.requests) == 3
        m.assert_called_with(
            url=f"{settings.PHABRICATOR_URL}/api/maniphest.edit",
            method="post",
            data=build_request_data(
                {
                    "objectIdentifier": expected_phid,
                    "transactions": expected_transactions,
                },
            ),
        )
