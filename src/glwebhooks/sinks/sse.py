# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import json
import logging

import quart

from .. import metrics

logger = logging.getLogger(__name__)

sse_blueprint = quart.Blueprint("sse", __name__, url_prefix="/sse")


class ServerSentEvent:
    def __init__(self, event=None, comment=None, data=None):
        self.event = event
        self.comment = comment
        self.data = data
        self.encoded = None

    def encode(self):
        """Encode as bytearray."""
        if not self.encoded:
            msg = []
            if self.comment is not None:
                msg.append(f": {self.comment}")
            if self.event is not None:
                msg.append(f"event: {self.event}")
            if self.data is not None:
                if not isinstance(self.data, str):
                    self.data = json.dumps(self.data)
                for line in self.data.splitlines():
                    msg.append(f"data: {line}")
            if msg:
                msg_str = "\n".join(msg)
                self.encoded = f"{msg_str}\n\n".encode()
            else:
                self.encoded = b":empty packet\n\n"
        return self.encoded


@sse_blueprint.before_app_serving
def initialize_clients():
    sse_blueprint.clients = set()


@sse_blueprint.get("/")
async def eventstream():
    """Connect to a text/event-stream feed of GitLab events."""
    metrics.SSE_CLIENTS_SEEN.inc({"path": "/sse/"})

    if "text/event-stream" not in quart.request.accept_mimetypes:
        return "SSE is only available as text/event-stream", 406

    queue = asyncio.Queue()
    sse_blueprint.clients.add(queue)
    logger.info("Accepted connection %s", hash(queue))

    async def event_queue_iterator():
        """Emit SSE encoded records."""
        metrics.SSE_CLIENTS_CONNECTED.inc({"path": "/sse/"})
        loop = asyncio.get_running_loop()
        packet = ServerSentEvent(
            event="keepalive",
            comment="Initial connect ping",
            data={"keepalive": loop.time()},
        )
        yield packet.encode()

        while True:
            try:
                async with asyncio.timeout(61):
                    packet = await queue.get()
            except TimeoutError:
                packet = ServerSentEvent(
                    event="keepalive",
                    comment="Queue get timeout ping",
                    data={"keepalive": loop.time()},
                )
            except asyncio.CancelledError:
                # Client has disconnected
                logger.info("Disconnect from %s", hash(queue))
                sse_blueprint.clients.remove(queue)
                metrics.SSE_CLIENTS_CONNECTED.dec({"path": "/sse/"})
                raise  # re-raise CancelledError

            if packet.event != "keepalive":
                metrics.EVENTS_RELAYED.inc(
                    {"type": packet.event or "event"},
                )
            yield packet.encode()

    response = await quart.make_response(
        event_queue_iterator(),
        {
            "Content-Type": "text/event-stream",
            "Cache-Control": "no-cache",
            "Transfer-Encoding": "chunked",
            "Connection": "keep-alive",
            "X-Accel-Buffering": "no",
        },
    )
    response.timeout = None
    return response


class SSESink:
    """Sink to route GitLab events to active server-sent event clients."""

    def __init__(self, quart_app):
        quart_app.register_blueprint(sse_blueprint)

    async def notify(self, event):
        """Rebroadcast event by queuing for all attached clients."""
        if not sse_blueprint.clients:
            # Nobody to tell
            return

        packet = ServerSentEvent(
            event="webhook",
            data=event,
        )
        packet.encode()
        logger.debug("Notifying %d clients", len(sse_blueprint.clients))
        for client in sse_blueprint.clients:
            client.put_nowait(packet)
            await asyncio.sleep(0.001)
