# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
import time

import gitlab
import requests

from .. import gitreviewers
from .. import settings

# FIXME: Filter out changes made by l10n-bot
logger = logging.getLogger(__name__)


class GitlabMentionsSink:
    CHANGE_MERGE_REQUEST_SUBSCRIPTION_MUTATION = """
mutation mergeRequestSetSubscription($fullPath: ID!, $iid: String!, $subscribedState: Boolean!) {
  updateIssuableSubscription: mergeRequestSetSubscription(
    input: {projectPath: $fullPath, iid: $iid, subscribedState: $subscribedState}
  ) {
    issuable: mergeRequest {
      id
      subscribed
      __typename
    }
    errors
    __typename
  }
}
    """

    def __init__(self, quart_app):  # noqa: U100 unused argument
        self.url = settings.GITLAB_URL
        self.token = settings.GITLAB_TOKEN
        if not self.url:
            raise Exception(
                "The gitlab-mentions sink requires GITLAB_URL "
                "pointing to the Gitlab server",
            )
        if not self.token:
            raise Exception(
                "The gitlab-mentions sink requires GITLAB_TOKEN "
                "containing an access token with the ability to add "
                "notes to merge requests",
            )

        self.gl = gitlab.Gitlab(
            self.url,
            private_token=self.token,
            user_agent="gitlab-webhooks-gitlab-mentions-sink",
        )
        self.gl.auth()

        self.reviewers = []
        self.reviewers_mtime = 0
        self.reviewers_ttl = settings.GITLAB_REVIEWERS_TTL

    def notify(self, event):
        kind = event.get("object_kind")
        object_attributes = event.get("object_attributes")
        if kind != "merge_request" or object_attributes is None:
            return
        if object_attributes.get("action") not in [
            "open",
            "reopen",
            "update",
        ]:
            return

        try:
            (
                mr,
                project_path,
                changed_files,
                added_files,
                existing_mentions,
            ) = self.extract_event_info(
                event,
            )
        except gitlab.exceptions.GitlabGetError as e:
            # This will happen if we're processing an event about a private project.
            if e.error_message == "404 Project Not Found":
                return

        interested_reviewers = [
            reviewer.name.lower()
            for reviewer in self.get_reviewers()
            if reviewer.match(project_path, changed_files, added_files)
        ]

        new_mentions = set(interested_reviewers) - set(existing_mentions)

        if new_mentions:
            note = " ".join(
                [f"@{mention}" for mention in new_mentions],
            )
            logger.info(
                "GitlabMentionsSink.notify() adding the following note to "
                "%s: %s",
                object_attributes["url"],
                note,
            )
            mr.notes.create({"body": note})
            logger.info(
                "GitlabMentionsSink.notify() unsubscribing from %s!%s",
                project_path,
                mr.iid,
            )
            self.unsubscribe(mr, project_path)

    def extract_event_info(self, event):
        mr = self.gl.projects.get(
            event["project"]["id"],
            lazy=True,
        ).mergerequests.get(
            event["object_attributes"]["iid"],
            lazy=True,
        )

        added_files = []
        changed_files = []

        for change in mr.changes()["changes"]:
            changed_files.append(change["new_path"])
            if change["new_file"]:
                added_files.append(change["new_path"])

        return (
            mr,
            event["project"]["path_with_namespace"],
            changed_files,
            added_files,
            [participant["username"] for participant in mr.participants()],
        )

    def get_reviewers(self):
        if time.time() - self.reviewers_mtime >= self.reviewers_ttl:
            # Cache expired. Refresh.
            self.reviewers = gitreviewers.parse_Git_Reviewers()
            self.reviewers_mtime = time.time()

        return self.reviewers

    def unsubscribe(self, mr, project_path):
        # mr.unsubscribe() should work, but it doesn't.
        # Using GRAPHQL does work.
        payload = {
            "operationName": "mergeRequestSetSubscription",
            "variables": {
                "fullPath": project_path,
                "iid": str(mr.iid),
                "subscribedState": False,
            },
            "query": GitlabMentionsSink.CHANGE_MERGE_REQUEST_SUBSCRIPTION_MUTATION,
        }

        resp = requests.post(
            f"{self.url}/api/graphql",
            headers={
                "Content-Type": "application/json",
                "PRIVATE-TOKEN": self.token,
            },
            json=payload,
        )
        resp.raise_for_status()

        resp = resp.json()
        if resp.get("errors"):
            raise Exception(
                "\n".join(error["message"] for error in resp["errors"]),
            )
