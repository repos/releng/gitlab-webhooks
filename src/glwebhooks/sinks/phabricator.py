# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import logging

from .. import settings
from .. import utils
from ..phab import Client

PROJECT_PATCH_FOR_REVIEW_PHID = "PHID-PROJ-onnxucoedheq3jevknyr"
logger = logging.getLogger(__name__)


class PhabricatorSink:
    def __init__(self, quart_app):  # noqa: U100 unused argument
        self.url = settings.PHABRICATOR_URL
        self.user = settings.PHABRICATOR_USER
        self.token = settings.PHABRICATOR_TOKEN

        if not self.url:
            raise RuntimeError(
                "PHABRICATOR_URL must be populated for the phabricator sink",
            )
        if not self.user:
            raise RuntimeError(
                "PHABRICATOR_USER must be populated for the phabricator sink",
            )
        if not self.token:
            raise RuntimeError(
                "PHABRICATOR_TOKEN must be populated for the phabricator sink",
            )

        self.client = Client(self.url, self.user, self.token)

    async def notify(self, event):
        kind = event.get("object_kind")
        logger.debug("Received event of kind %s", kind)

        # if kind == 'push':
        #   self._add_pushed_comments(event)
        # if kind == 'tag_push':
        #     self._add_tagged_comments(event)
        if kind == "merge_request":
            await self._handle_mr_event(event)

    async def _add_pushed_comments(self, event):
        # Decide if we want to notify for this event:
        after = event.get("after", None)
        before = event.get("before", None)
        if before == "0000000000000000000000000000000000000000":
            # New branch, don't notify:
            return
        if after == before:
            # No changes, don't notify (unclear whether this can actually
            # happen):
            return

        for commit in event.get("commits"):
            message = self._push_message(commit, event)
            for task_id in utils.collect_task_ids(commit.get("message", "")):
                await self.client.edit_task(task_id, comment=message)

    async def _handle_mr_event(self, event):
        object_attributes = event.get("object_attributes")
        if object_attributes is None:
            return
        action = object_attributes.get("action")
        if action is None:
            return

        if action in ["open", "close", "reopen", "merge"]:
            for task_id in utils.get_all_potential_task_ids_from_mr(
                object_attributes,
            ):
                await self._add_mr_comment(task_id, action, event)

        elif action == "update":
            # Notes:
            #
            # An MR change event is structured like so:
            #
            # {'changes': {'description': {'current': 'Adds ci to run tests\n\nBug: #323225',
            #                              'previous': 'Adds ci to run tests'},
            #              'last_edited_at': {'current': '2022-12-13 05:12:53 UTC',
            #                                 'previous': None},
            #              'last_edited_by_id': {'current': 12, 'previous': None},
            #              'updated_at': {'current': '2022-12-13 05:12:53 UTC',
            #                             'previous': '2022-12-13 05:09:52 UTC'},
            #              'updated_by_id': {'current': 12, 'previous': None}},
            #
            # We aren't checking commit messages here whatsoever.

            changes = event.get("changes")
            for task_id in utils.get_new_task_ids_from_mr_changes(changes):
                await self._add_mr_comment(task_id, action, event)

    async def _add_mr_comment(self, task_id, action, event):
        message = self._generate_mr_comment(task_id, action, event)

        # It is okay to attempt to add a project that has already been
        # added.  This just results in a null operation.
        add_project_phid = (
            PROJECT_PATCH_FOR_REVIEW_PHID
            if action
            in [
                "open",
                "reopen",
            ]
            else None
        )

        await self.client.edit_task(
            task_id,
            comment=message,
            add_project_phid=add_project_phid,
        )
        logger.info("Added a comment on task %s", task_id)

    def _push_message(self, commit, event) -> str:
        user = (
            event.get("user_username")
            or event.get(
                "user_name",
            )
            or "USERNAME MISSING"
        )

        project = event.get("project")
        repo = "REPO INFORMATION MISSING"
        repo_url = "REPO URL MISSING"

        if project:
            x = project.get("path_with_namespace")
            if x:
                repo = x
            x = project.get("http_url") or project.get("web_url")
            if x:
                repo_url = x

        branch = utils.get_branch_from_ref(event["ref"])
        ref = commit["id"]
        title = commit["title"]
        link = commit["url"]
        author = commit["author"]["name"]
        return (
            f"Related commit [[{link} | {ref[:8]}]] "
            f"pushed by {user} (author: {author}):\n\n"
            f"[ [[{repo_url} | {repo}]]@{branch} ] {title}\n"
        )

    def _generate_mr_comment(
        self,
        task_id,  # noqa: U100 unused argument
        action,
        event,
    ) -> str:
        object_attributes = event.get("object_attributes")
        past_tense_action = utils.make_past_tense(action)

        who = event.get("user", {"username": "MISSING USER INFO"}).get(
            "username",
            "MISSING USERNAME",
        )
        mr_url = object_attributes.get("url", "MISSING URL")
        title = object_attributes.get("title", "MISSING TITLE")

        # FIXME: Mention source and target branches
        return f"{who} {past_tense_action} {mr_url}\n\n{title}\n"
