# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import pathlib

import environ

env = environ.Env()
env.smart_cast = False
environ.Env.read_env(env_file=pathlib.Path.cwd() / ".env")

# Full path (or relative to runtime cwd) to log file for seen events.
LOG_EVENTS_TO = env.str("LOG_EVENTS_TO", default=None)

# Enable debug output
DEBUG = env.bool("DEBUG", default=False)

# Expected X-Gitlab-Token value to verify input integrity.
SOURCE_TOKEN = env.str("SOURCE_TOKEN", default=None)

# List of output sinks to enable.
SINKS_ENABLED = env.list("SINKS_ENABLED", default=[])

# URL to Phabricator server
PHABRICATOR_URL = env.str(
    "PHABRICATOR_URL",
    default="https://phabricator.wikimedia.org",
)
# Phabricator username
PHABRICATOR_USER = env.str("PHABRICATOR_USER", default="CodeReviewBot")
# Phabricator Conduit bearer token
PHABRICATOR_TOKEN = env.str("PHABRICATOR_TOKEN", default=None)

# URL to GitLab server
GITLAB_URL = env.str("GITLAB_URL", default="https://gitlab.wikimedia.org")
# GitLab personal access token with the ability to add notes to merge requests
GITLAB_TOKEN = env.str("GITLAB_TOKEN", default=None)
# Time to live in seconds for cached [[mw:Git/Reviewers]] content
GITLAB_REVIEWERS_TTL = env.int("GITLAB_REVIEWERS_TTL", default=300)

# Local interface and port to bind webserver to
WEB_BIND = env.list("WEB_BIND", default=["0.0.0.0:8000"])
