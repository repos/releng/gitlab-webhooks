# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
from fnmatch import fnmatch
import logging
import re

import lxml.objectify
import requests


class GitReviewer:
    def __init__(
        self,
        project_pattern,
        name,
        filere,
        matchall,
        only_match_new_files,
    ):
        self.project_pattern = project_pattern
        self.name = name
        self.filere = filere
        self.only_match_new_files = only_match_new_files
        self.filter = all if matchall else any

    def __repr__(self) -> str:
        return f"<GitReviewer project: {self.project_pattern}, reviewer: {self.name}, files: {self.filere}>"

    def match(self, project_path, changed_files, added_files) -> bool:
        if not fnmatch(project_path, self.project_pattern):
            return False

        if self.only_match_new_files:
            changed_files = added_files

        return self.filter(self.filere.search(file) for file in changed_files)


# This function is a modified form of
# https://github.com/valhallasw/gerrit-reviewer-bot/blob/master/add_reviewer.py,
# so credit goes to Merlijn van Deen (valhallasw).
def parse_Git_Reviewers():  # noqa: N802 lowercase function name
    data = requests.get(
        "https://www.mediawiki.org/w/api.php?format=json&action=parse&page=Git/Reviewers&prop=parsetree",
    ).json()
    tree = lxml.objectify.fromstring(data["parse"]["parsetree"]["*"])

    nofilere = re.compile("")

    res = []

    for section in tree.iter("h"):
        name = section.text.strip("= ")
        for sibling in section.itersiblings():
            if sibling.tag == "h":
                break
            if (
                sibling.tag == "template"
                and sibling.title == "Gitlab-mention"
            ):
                reviewer = None
                filere = nofilere
                matchall = False
                only_match_new_files = False

                for part in sibling.iter("part"):
                    if part.name == "" and part.name.attrib["index"] == "1":
                        reviewer = part.value.text
                    elif part.name == "file_regexp":
                        try:
                            filere = re.compile(
                                part.value.text or part.value.ext.inner.text,
                                flags=re.DOTALL | re.IGNORECASE,
                            )
                        except re.error:
                            logging.error(
                                "Could not process file regexp %r -- ignoring.",
                                (
                                    part.value.text
                                    or part.value.ext.inner.text,
                                ),
                            )
                    elif (
                        part.name == "match_all_files"
                        or part.value.text == "match_all_files"
                    ):
                        matchall = True
                    elif (
                        part.name == "only_match_new_files"
                        or part.value.text == "only_match_new_files"
                    ):
                        only_match_new_files = True

                res.append(
                    GitReviewer(
                        name,
                        reviewer,
                        filere,
                        matchall,
                        only_match_new_files,
                    ),
                )

    return res
