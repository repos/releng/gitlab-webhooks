# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import asyncio
import datetime
import json
import logging
import time

import aiorun
import hypercorn
import quart

from . import metrics
from . import settings
from .sinks.gitlabmentions import GitlabMentionsSink
from .sinks.phabricator import PhabricatorSink
from .sinks.sse import SSESink
from .version import __version__

app = quart.Quart(__name__)
app.config.from_prefixed_env()

metrics.add_middleware(app)

config = {
    "debug": settings.DEBUG,
    "log_events_to": settings.LOG_EVENTS_TO,
    "source_token": settings.SOURCE_TOKEN,
    "sinks": [],
}

# TODO:
# * Robust error handling, handling of missing attributes (some work done, could use a bit more)
# * Deliver asynchronously (decouple event collection from event handling)


@app.context_processor
def inject_version():
    return {"version": __version__}


@app.template_filter()
def format_time(value, fmt="%Y-%m-%d %H:%M:%S"):
    return time.strftime(fmt, time.gmtime(value))


@app.template_filter()
def human_time(value):
    now = datetime.datetime.now()
    diff = now - datetime.datetime.fromtimestamp(value)

    if diff.days == 0:
        if diff.seconds < 10:
            return "just now"
        if diff.seconds < 60:
            return f"{int(diff.seconds)} seconds ago"
        if diff.seconds < 120:
            return "a minute ago"
        if diff.seconds < 3600:
            return f"{int(diff.seconds / 60)} minutes ago"
        if diff.seconds < 7200:
            return "an hour ago"
        if diff.seconds < 86400:
            return f"{int(diff.seconds / 3600)} hours ago"
    if diff.days == 1:
        return "a day ago"
    if diff.days < 7:
        return f"{diff.days} days ago"
    if diff.days < 31:
        return f"{int(diff.days / 7)} weeks ago"
    if diff.days < 365:
        return f"{int(diff.days / 30)} months ago"
    return f"{int(diff.days / 365)} years ago"


@app.get("/")
async def home():
    ctx = {
        "last_seen": metrics.LAST_SEEN_TIMES,
        "boot_time": metrics.BOOT_TIME,
    }
    return await quart.render_template("index.html", **ctx)


@app.get("/healthz")
async def healthz():
    """Trivial liveness check."""
    metrics.record_last_seen("/healthz")
    return {"status": "OK"}, 200


@app.post("/hooks")
async def hooks():
    metrics.record_last_seen("/hooks")
    await quart.request.get_data()
    event = await quart.request.json
    metrics.EVENTS_RECEIVED.inc({"path": "/hooks"})

    if config["debug"]:
        app.logger.debug("Event: %s", event)

    if config["log_events_to"]:
        with open(config["log_events_to"], "a") as f:
            json.dump(event, f)
            f.write("\n")

    if (
        config["source_token"]
        and quart.request.headers.get("X-Gitlab-Token")
        != config["source_token"]
    ):
        metrics.EVENTS_REJECTED.inc({"path": "/hooks"})
        return "Invalid token", 400

    for sink in config["sinks"]:
        try:
            if asyncio.iscoroutinefunction(sink.notify):
                await sink.notify(event)
            else:
                # Run legacy synchronous code in a thread
                await app.sync_to_async(sink.notify)(event)
        except Exception:
            app.logger.exception("Notification to %s failed.", sink)

    return ""


SINKS_MAP = {
    "phabricator": PhabricatorSink,
    "gitlab-mentions": GitlabMentionsSink,
    "sse": SSESink,
}


class HypercornLogger(hypercorn.logging.Logger):
    """Configure logging for hypercorn."""

    def __init__(self, config):
        self.access_log_format = config.access_log_format
        self.access_logger = logging.getLogger("hypercorn.access")
        self.error_logger = logging.getLogger("hypercorn.error")

    async def access(self, request, response, request_time):
        if self.access_logger is not None:
            # Hack for https://github.com/pgjones/hypercorn/issues/158
            if response is not None:
                self.access_logger.info(
                    self.access_log_format,
                    self.atoms(request, response, request_time),
                )


async def async_main():
    if config["debug"]:
        app.logger.info("Enabling debug mode (from config)")

    if config["log_events_to"]:
        app.logger.info(
            "Will log event records to %s",
            config["log_events_to"],
        )

    if not settings.SINKS_ENABLED:
        app.logger.warning(
            "No sinks defined.  Nothing will happen for incoming events",
        )
    else:
        for sink_type in settings.SINKS_ENABLED:
            sink_constructor = SINKS_MAP.get(sink_type)
            if not sink_constructor:
                raise RuntimeError(f"Unsupported sink type: {type}")
            app.logger.info("Adding %s sink.", sink_type)
            config["sinks"].append(sink_constructor(app))

    app.logger.info("Starting webservice.")
    await hypercorn.asyncio.serve(
        app,
        hypercorn.config.Config.from_mapping(
            bind=settings.WEB_BIND,
            graceful_timeout=7,
            include_server_header=False,
            logger_class=HypercornLogger,
            use_reloader=False,
            websocket_ping_interval=5,
            worker_class="asyncio",
            workers=1,
        ),
        mode="asgi",
        shutdown_trigger=lambda: asyncio.Future(),
    )


def main():
    parser = argparse.ArgumentParser(description="gitlab-webhooks")
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        dest="loglevel",
        help="Increase logging verbosity",
    )
    args = parser.parse_args()

    logging.basicConfig(
        level=max(logging.DEBUG, logging.WARNING - (10 * args.loglevel)),
        format="%(asctime)s %(name)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
    )
    logging.captureWarnings(True)

    return aiorun.run(
        async_main(),
        stop_on_unhandled_errors=True,
        timeout_task_shutdown=31,
    )


if __name__ == "__main__":
    main()
