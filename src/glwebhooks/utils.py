# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import re


def get_branch_from_ref(ref: str):
    """pull branch out of a refs/heads/* string, or return None"""
    if ref.startswith("refs/heads/"):
        return ref[len("refs/heads/") :]
    return None


def collect_task_ids(message: str) -> set:
    """Returns a set of task id strings"""
    return set(
        re.findall(r"^Bug:\s*(T\d+)\s*$", message, re.MULTILINE),
    )


def get_all_potential_task_ids_from_mr(object_attributes: dict) -> set:
    """Returns a set of task id strings"""
    desc = object_attributes.get("description")
    title = object_attributes.get("title")
    if desc is None:
        desc = ""
    if title is None:
        title = ""

    # It is possible for a merge request to be created without any
    # associated commits (for example if the source branch does not
    # exist yet).  This is a normal scenario for merge requests created by
    # GerritLab.
    last_commit = object_attributes.get("last_commit")
    last_commit_message = (
        object_attributes["last_commit"]["message"] if last_commit else ""
    )

    return (
        collect_task_ids(desc)
        .union(collect_task_ids(title))
        .union(collect_task_ids(last_commit_message))
    )


def get_new_task_ids_from_mr_changes(changes):
    """Returns a set of task id strings from a changed merge request."""
    task_ids_previous = set()
    task_ids_current = set()

    if changes.get("description"):
        task_ids_previous.update(
            collect_task_ids(
                changes["description"]["previous"],
            ),
        )
        task_ids_current.update(
            collect_task_ids(
                changes["description"]["current"],
            ),
        )

    if changes.get("title"):
        task_ids_previous.update(
            collect_task_ids(changes["title"]["previous"]),
        )
        task_ids_current.update(collect_task_ids(changes["title"]["current"]))

    return task_ids_current.difference(task_ids_previous)


def make_past_tense(word: str) -> str:
    if word[-1] == "e":
        return word + "d"
    else:
        return word + "ed"
