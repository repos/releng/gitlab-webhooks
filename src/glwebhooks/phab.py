#
# This file is part of bd808's stashbot application
# Copyright (C) 2016 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import logging

import aiohttp

USER_AGENT = "{name} ({url}) python-aiohttp/{vers}".format(
    name="gitlab-webhooks",
    url="https://wikitech.wikimedia.org/wiki/GitLab/Webhooks",
    vers=aiohttp.__version__,
)

logger = logging.getLogger(__name__)


class Client:
    """Phabricator client"""

    def __init__(self, url, username, key):
        self.url = url
        self.username = username
        self.session = {"token": key}

    def _create_session(self):
        return aiohttp.ClientSession(
            connector=aiohttp.TCPConnector(keepalive_timeout=37),
            headers={"User-Agent": USER_AGENT},
            trust_env=True,
            timeout=aiohttp.ClientTimeout(connect=3, sock_connect=3),
        )

    async def post(self, path, data):
        async with self._create_session() as sess:
            data["__conduit__"] = self.session
            url = f"{self.url}/api/{path}"
            logger.debug("POST %s (%s)", url, data)
            async with sess.post(
                url,
                data={"params": json.dumps(data), "output": "json"},
            ) as r:
                resp = await r.json()
                logger.debug("POST %s (%s): %s", url, data, resp)
                if resp["error_code"] is not None:
                    raise Exception(resp["error_info"])
                return resp["result"]

    async def lookup_phid(self, label):
        """
        Returns the object identifier of the Phab task with the
        specified label (e.g. "T123456").  If the task is not
        found, returns None.
        """
        r = await self.post("phid.lookup", {"names": [label]})
        # If the task is not found, an empty list is returned.
        # If the task is found, a dictionary is returned.
        if not r:
            return None
        obj = r.get(label)
        if obj is None or obj["type"] != "TASK":
            return None

        # T180081: Ensure that we don't leak information about
        # security tasks even if the bot somehow has access to the
        # task.
        info = await self.task_details(obj["phid"])
        aux = info.get("auxiliary", {})
        st = aux.get("std:maniphest:security_topic")
        if st and st != "default":
            return None

        return obj

    async def task_details(self, phid):
        """Lookup details of a Maniphest task."""
        r = await self.post("maniphest.query", {"phids": [phid]})
        if phid in r:
            return r[phid]
        raise Exception("No task found for phid %s" % phid)

    async def edit_task(
        self,
        task,
        *,
        comment: str | None = None,
        add_project_phid: str | None = None,
    ) -> bool:
        """Edit the specified task by optionally adding a comment
           and/or optionally adding a project to the task.
        :param task: Task number (e.g. T12345)
        :param comment: Comment to add to task
        :param add_project_phid: Project to add to the task
        :returns edited: True if the task was found and updated.  False if not found.
        :rtype: bool
        """

        task_info = await self.lookup_phid(task)
        if task_info is None:
            return False
        phid = task_info["phid"]
        if phid is None:
            return False

        transactions = []
        if comment:
            transactions.append({"type": "comment", "value": comment})
        if add_project_phid:
            transactions.append(
                {"type": "projects.add", "value": [add_project_phid]},
            )

        if len(transactions) == 0:
            # Nothing to do!
            return

        await self.post(
            "maniphest.edit",
            {
                "objectIdentifier": phid,
                "transactions": transactions,
            },
        )

        return True
