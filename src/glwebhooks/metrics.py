# This file is part of RelEng's gitlab-webhooks application
# Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
# Copyright (C) 2024 Wikimedia Foundation and contributors.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
import time

import aioprometheus
import aioprometheus.asgi.quart

EVENTS_RECEIVED = aioprometheus.Counter(
    "events_received",
    "Number of events received",
)

EVENTS_REJECTED = aioprometheus.Counter(
    "events_rejected",
    "Number of events rejected",
)

EVENTS_RELAYED = aioprometheus.Counter(
    "events_relayed",
    "Number of events relayed via server-sent events",
)

SSE_CLIENTS_CONNECTED = aioprometheus.Gauge(
    "sse_clients_connected",
    "Number of server-sent events clients actively connected",
)

SSE_CLIENTS_SEEN = aioprometheus.Counter(
    "sse_clients_seen",
    "Number of server-sent events clients ever connected",
)

LAST_SEEN_TIMES = {}

BOOT_TIME = time.time()


def add_middleware(app):
    """Wrap a Quart app in Prometheus middleware and expose /metrics."""
    app.asgi_app = aioprometheus.MetricsMiddleware(
        app=app.asgi_app,
        exclude_paths=aioprometheus.asgi.middleware.EXCLUDE_PATHS
        + ("/healthz",),
    )

    app.add_url_rule(
        "/metrics",
        "metrics",
        aioprometheus.asgi.quart.metrics,
        methods=["GET"],
    )
    return app


def record_last_seen(thing):
    """Record a unix timestamp in LAST_SEEN_TIMES."""
    LAST_SEEN_TIMES[thing] = time.time()
