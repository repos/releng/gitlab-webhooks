# Gitlab Webhooks

A webhook handler for performing actions on other systems in response
to GitLab events.

Possible event actions:

* Posting Phabricator comments when a merge request with a "Bug:"
  footer is created, merged, or closed.

* Adding mentions to a GitLab merge request based on the contents of
  https://www.mediawiki.org/wiki/Git/Reviewers

See https://wikitech.wikimedia.org/wiki/GitLab/Webhooks for further details.

## COPYING

Copyright (C) 2024 Ahmon Dancy, Brennen Bearnes, and contributors
Copyright (C) 2024 Wikimedia Foundation and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
